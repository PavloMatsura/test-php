<?php
include_once("core/DB.php");

class Model
{
    public function get_country_name($page_id)
    {
        $sql = 'SELECT table_1.t1_string, table_2.t2_string, table_3.t3_string
                FROM table_1
                LEFT JOIN table_2 ON table_1.t1_id = table_2.t2_t1_id
                LEFT JOIN table_3 ON table_2.t2_id = table_3.t3_t2_id
                WHERE table_1.t1_id = ' . $page_id;

        DB::query($sql);

        $result = DB::get_array();

        return $result;
    }
}
