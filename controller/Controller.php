<?php
include_once("model/Model.php");

class Controller
{
    public $connection;

    public function __construct()
    {
        $this->connection = new Model();
    }

    public function get_phrase()
    {
        if (isset($_GET['page_id'])) {
            $page_id = (int)$_GET['page_id'];
            $country_name = $this->connection->get_country_name($page_id);

            include 'view/country.php';
        }
    }
}
