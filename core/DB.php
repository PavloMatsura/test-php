<?php

class DB
{
    /** @var PDO */
    private static $dbc;

    /** @var \PDOStatement|null */
    private static $request;

    /**
     * @return PDO
     */
    private static function connect() {
        if (static::$dbc) {
            return static::$dbc;
        }

        static::$dbc = new PDO('mysql:host=localhost;port=3306;dbname=test', 'root', '', [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_PERSISTENT => false,
        ]);
        static::$dbc->exec('SET NAMES utf8');

        return static::$dbc;
    }

    public static function query($sql)
    {
        $db = self::connect();

        self::$request = $db->prepare($sql);

        return self::$request;
    }

    public static function get_array()
    {
        return self::$request->fetchAll();
    }
}
